from .action import Action2, Action3
from mud.events import UseWithEvent

class UseWithAction(Action3):
    EVENT = UseWithEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    RESOLVE_OBJECT2 = "resolve_for_use"
    ACTION = "use-with"