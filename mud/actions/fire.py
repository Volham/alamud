from .action import Action2, Action3
from mud.events import FireWithEvent

class FireWithAction(Action3):
    EVENT = FireWithEvent
    ACTION = "fire-with"
    RESOLVE_OBJECT = "resolve_for_operate"
    RESOLVE_OBJECT2 = "resolve_for_use"
