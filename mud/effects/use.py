from .effect import Effect2, Effect3
from mud.events import UseWithEvent

class UseWithEffect(Effect3):
    EVENT = UseWithEvent