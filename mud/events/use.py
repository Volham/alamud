from .event import Event2, Event3

class UseWithEvent(Event3):
    NAME = "use-with"

    def perform(self):
        self.inform("use-with")